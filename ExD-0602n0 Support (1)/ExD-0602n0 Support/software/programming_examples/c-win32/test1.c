
/* Copyright (C) nanoFaktur GmbH - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Cunman Zhang <cunman.zhang@nanoFaktur.com>, Feb. 2018
 */

/*
Function description:
  simple code for loop testing
*/

#include <windows.h>
#include <stdio.h>
#include <conio.h>      // for getch()
#include <string.h>
#include "nF_interface.h" // Functions definition

#define SVO_OFF             0
#define SVO_ON              1
#define MAX_AXIS            3

int main(void)
{
    int fd; 
    int axis[MAX_AXIS] = {0, 1, 2}; // Index of axis0 to axis2
    int bSvoOn[MAX_AXIS] = { SVO_ON , SVO_ON , SVO_ON  }; // Close-loop mode setting
    int i, num;
    float target[MAX_AXIS], position[MAX_AXIS];

    printf("DLL revision: %f\n", nF_get_dll_revision() );
    nF_set_log_state(1); // enable command log: optional
    printf("Note: controller responses can be found in nF_interface_log.txt.\n");
    
    fd = nF_intf_connect_com(8); // Connect to COM8
    if( fd < 0) {
        printf("Can't open interface");
        return -1;
    }
    
    num = 1; // controlling only axis0
    
    nF_set_dev_axis_svo(num, &axis[0], &bSvoOn[0]);           // Set axis0 to close-loop
    // PID parameters should first be tuned for speed reason

    for(i=0; i<10; i++)  {
        target[0] = 10.0; // Set axis0 target here
        nF_set_dev_axis_target( SVO_ON, num, &axis[0], &target[0] ); 
        nF_get_dev_axis_position( num, &axis[0], &position[0] );
        printf( "Loop: %d, Target-axis0:%f, Position-axis0:%f\n", i, target[0], position[0] );
    }

    // for all 3 axes 
    num = MAX_AXIS;
    nF_set_dev_axis_svo(num, &axis[0], &bSvoOn[0]);
    for(i=0; i<10; i++)  {
        target[0] = 10.0; // Target for axis0
        target[1] = 10.0; // Target for axis1
        target[2] = 10.0; // Target for axis2
        nF_set_dev_axis_target( SVO_ON, num, &axis[0], &target[0] ); 
        nF_get_dev_axis_position( num, &axis[0], &position[0] );
        printf( "Loop: %d, Target-axis0:%f, Position-axis0:%f\n", i, target[0], position[0] );
        printf( "Loop: %d, Target-axis1:%f, Position-axis1:%f\n", i, target[1], position[1] );
        printf( "Loop: %d, Target-axis2:%f, Position-axis2:%f\n", i, target[2], position[2] );
    }

    nF_intf_disconnect( fd);

    printf("Press any key to quit.\n");
    getch();

    return 0;
}
