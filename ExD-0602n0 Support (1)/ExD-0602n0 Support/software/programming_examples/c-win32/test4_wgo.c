/* Copyright (C) nanoFaktur GmbH - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Cunman Zhang <cunman.zhang@nanoFaktur.com>, Jun. 2020
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "nF_interface.h"
#include "test4_wgo_func.h"

int main(void)
{
    int i, res=0;
    int axis, len, rate;
    float param[16];
    int cmdData[16];
    int wgo[3], wave[3], num;
    int interfaceId;
    
    // Note: 
    //    For High-End controller only
    //    the software should be linked to nF_interface.dll
    //    for close-loop, PID should already be tunned 

    printf("Wave-generator example...\n");
    nF_set_log_state(1);
 
    //-------------------------------------------------------
	// Connect to controller 
    //-------------------------------------------------------
    // Note: use the USB interface as example
    char sIPAddr[16];
    sprintf(sIPAddr, "192.168.168.168"); // USB interface
    interfaceId = nF_intf_connect_tcpip(sIPAddr);
    if(interfaceId<0){
        printf("Error: interface connection!\n");
        return -1; // error 
    }

    //-------------------------------------------------------
	// Step1: set wave-table
    //-------------------------------------------------------
	
	// Note: for more axes, repeat the flollowing process
    axis = 0; // index of first Axis
    
    // calculate length-of-wave-table/table-rate for 20Hz signal
    i_time2_len_rate(500.0, &len, &rate); // 500ms (=2.0Hz)
    i_set_wgo_rate(axis, rate);

    // Set the wave-table
    param[0] = 0.0;     // From
    param[1] = (float)len*0.5; // time-to-go-back, take 50% as example
    param[2] = 100.0;   // To
    i_wave_cfg(axis, WAVE_FORM_TRIANGLE, len, param, 3);
    
	// for setting each data points, 
	// float data[8192]; 
	// data[0]=0.0; data[1]=1.0; and so on ...
	//i_wave_cfg_pnt(axis, 0, 8192, &data[0]);
	
    //-------------------------------------------------------
    // Step2: configure wave-generator
    //-------------------------------------------------------
    wgo[0] = axis;	// Wave generator ID
    wave[0] = axis; // Wave table ID 
    num = 1; // for more axes, set the wgo[]/wave[]/num with right value 
    i_wgo_cfg(&wgo[0], &wave[0], num);

    // Set number of cycles: 0=continuously 
    for(i=0; i<num; i++) {
        cmdData[i*2] = axis;
        cmdData[i*2+1] = (u32)0;    // continuously 
    }    
    nF_intf_write_command_u32(CMD_ID_WGC, num*2, (u32*)cmdData);
    
    /*
    // configure recorder
    int recNum, recId[MAX_SEL_REC_NUM], recSrc[MAX_SEL_REC_NUM], recCh[MAX_SEL_REC_NUM];
    recId[0] = 0;
    recSrc[0] = REC_SRC_POS; // position (see command: ?0xFFF9)
    recCh[0] = axis; //which channel 
    recNum = 1;
    i_rec_cfg(&recId[0], &recSrc[0], &recCh[0], recNum);    
    */
    
    //-------------------------------------------------------
    // Step3: start wave-generator and/or recorder
    //-------------------------------------------------------
    i_start_event(NULL, 0);
    printf("Wave-generator started.\n");
    
    /*
    // read-back recorder data
    int recLen = nF_get_dev_rec_length(0);
    if(recLen >= 8192) {
        for(i=0; i<recNum; i++) {
            float *pBuf = malloc(); // TODO: prepare buffer for saving data  
            int recFrom = 0, recSize = 1024;
            res += nF_get_dev_rec_data(i, recFrom, recSize, pBuf); // TODO: loop for all data 
            //if(res) break; when error
        }
        // TODO: Show data or save to file
    }
    */

    //-------------------------------------------------------
    // End of operation
    //-------------------------------------------------------
    printf("Press <ENTER> to exit.\n");
    getchar();
    i_stop_wgo(&wgo[0], num, NULL);

    nF_intf_disconnect(interfaceId);
    return res;
}

