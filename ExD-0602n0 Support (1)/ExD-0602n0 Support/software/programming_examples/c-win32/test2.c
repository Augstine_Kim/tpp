
/* Copyright (C) nanoFaktur GmbH - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Cunman Zhang <cunman.zhang@nanoFaktur.com>, Feb. 2018
 */

/*
Function description:
  demo code for controller-simulation interface
*/

#include <windows.h>
#include <stdio.h>
#include <conio.h>  	// for getch()
#include <string.h>
#include "nF_interface.h"

int main(void)
{
    int fd; 
    int res;
    CommandCfg cmdCfg;
    ResponseMsg *pRspMsg=NULL;

    printf("DLL revision: %f\n", nF_get_dll_revision() );
    nF_set_log_state(1); // enable command log
    printf("Note: controller responses can be found in nF_interface_log.txt.\n");

    fd = nF_intf_connect_local("EBD-1202x0");  // device name
    if( fd < 0) {
        printf("Error: Can't connect to interface.\n");
        printf("Please start simulation software first.\n");
        return -1; 
    }

    // Example for reading from controller
    memset(&cmdCfg, 0x00, sizeof(CommandCfg) );
    cmdCfg.cmdId = 0xFFFF; // command ID: help 
    cmdCfg.customId = 0x2018; // what ever ...
    cmdCfg.option = CMD_PACKAGE_FLAG_RD | CMD_PACKAGE_FLAG_ACK;
    cmdCfg.dataNum = 0; // No parameters 

    // use buffer (pRspMsg) from DLL 
    res = nF_intf_read_command(fd, &cmdCfg, &pRspMsg);
    while( res == RSP_PACKAGE_MORE) {
        printf("Controller responses: (Number of data:%d)\n", pRspMsg->dataNum);
        res = nF_intf_read_more(fd, &pRspMsg);
    }
    printf("Controller responses status: %d\n", res);
    if(res < 0) return res;
    nF_free_rspMsg(pRspMsg);

    // Another method for sending command
    { u32 param[2];
    param[0] = 0; // recorder group-index
    param[1] = 1; // value for 'enable'
    nF_intf_write_command_u32(0x4040, 2, &param[0]); //recorder enable
    }

    printf("Last error code: %d\n", nF_get_dev_error() );

    nF_intf_disconnect(fd);

    printf("Press any key to quit.\n");
    getch();

    return 0;
}


