#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "nF_interface.h"
#include "test4_wgo_func.h"

//---------------------------------------------------------------------------
int i_time2_len_rate(double time_ms, int *pLen, int *pRate)
{   // calculate length/rate from time-value
   
    int rate, len;
    double tmp; // Note: float has problem

    tmp = (time_ms*SVO_RATE_KHZ + (MAX_DATA_LEN-1) )/MAX_DATA_LEN;
    rate = (int)(tmp) ;
    if(rate <= 0) rate = 1;
    
    tmp = (double)time_ms;
    tmp *= SVO_RATE_KHZ;
    len = (int)(tmp)/rate;
    if(len<=MIN_DATA_LEN) len = MIN_DATA_LEN;

    *pLen = len;
    *pRate = rate;
    return 0;
}

int i_set_wgo_rate(int ch, int rate)
{
    // set wgo table-rate
    ParamVal val;
    val.uData = rate;
    
    nF_set_dev_parameter_ram(ch, 0x31400000, CMD_DATA_FMT_U32, &val);
    return 0;
}

int i_set_rec_rate(int rate)
{
    // set recorder table-rate
    ParamVal val;
    val.uData = rate;

    nF_set_dev_parameter_ram(0, 0x40400000, CMD_DATA_FMT_U32, &val);
    //if( get_dev_type()!=DEV_TYPE_STD) nF_set_dev_parameter_ram(1, 0x40400000, CMD_DATA_FMT_U32, &val);
    return 0;
}

int i_wave_cfg(int waveId, int form, int len, float *param, int paramNum)
{
    // configure the wave table    
    int res, i;
    float CmdData[16];

    res = nF_intf_write_command_u32(CMD_ID_WCR, 1, (u32*)&waveId); // Clear wave
    if( res < 0) return res;

    CmdData[0] = (float)waveId;
    CmdData[1] = 0.0; // mode: NEW
    CmdData[2] = (float)form; // 
    CmdData[3] = 0.0; // x0
    CmdData[4] = (float)len; // segLen

    for(i=0; i<paramNum; i++) CmdData[5+i] = param[i];
    res = nF_intf_write_command_float(CMD_ID_WAV, 5+paramNum, CmdData);
    return res;
}

int i_wave_cfg_pnt(int waveId, int from, int len, float *param)
{
    // config wave table with data (buffer: param)    
    #define MAX_WAVE_PNT1      100
    int res, i;
    float CmdData[MAX_WAVE_PNT1+8], *pRd;
    int left, sent, offset;

    res = nF_intf_write_command_u32(CMD_ID_WCR, 1, (u32*)&waveId); // Clear wave table
    if( res < 0) return res;

    left = len;
    offset = from;
    pRd = param; // param is the buffer address of input data

    while(left) {
        sent = (left>MAX_WAVE_PNT1)? MAX_WAVE_PNT1 : left;

        CmdData[0] = (float)waveId;
        CmdData[1] = 0.0; // mode: NEW
        CmdData[2] = (float)0; // 0=PNT 
        CmdData[3] = (float)offset; // x0
        CmdData[4] = (float)sent;
        for(i=0; i<sent; i++) CmdData[5+i] = *pRd++;
        res = nF_intf_write_command_float(CMD_ID_WAV, 5+sent, CmdData);
        if(res) break;

        left -= sent;
        offset += sent;
    }

    return res;
}

int i_wgo_cfg(int *wgo, int *wave, int wgoNum)
{
    // connect WGO to wave-table
    int i, res, chNum = 3; /* get_max_axes_num(); */
    u32 CmdData[16];
    
    CmdData[0] = 0;
    CmdData[1] = 0; // disable
    res = nF_intf_write_command_u32(CMD_ID_EST, 2, CmdData); // Reset event
    if( res < 0) return res;

    for(i=0; i<chNum; i++) {
        CmdData[i*2] = i;
        CmdData[i*2+1] = 0;
    }
    res = nF_intf_write_command_u32(CMD_ID_WGO, 2*chNum, CmdData); // Stop WGO
    if( res < 0) return res;

    if(!wgoNum) return 0;

    for(i=0; i<wgoNum; i++) {
        CmdData[i*2] = wgo[i];
        CmdData[i*2+1] = 1;
    }
    res = nF_intf_write_command_u32(CMD_ID_HVE, 2*wgoNum, CmdData); // HV enable
    if( res < 0) return res;

    for(i=0; i<wgoNum; i++) {
        CmdData[i*2] = wgo[i];
        CmdData[i*2+1] = 1; // Target = WGO
    }
    res = nF_intf_write_command_u32(CMD_ID_TSL, 2*wgoNum, CmdData); // Target to WGO
    if( res < 0) return res;

    for(i=0; i<wgoNum; i++) {
        CmdData[i*2] = wgo[i];
        CmdData[i*2+1] = wave[i];
    }
    res = nF_intf_write_command_u32(CMD_ID_WSL, 2*wgoNum, CmdData); // WGO data to wave-table
    if( res < 0) return res;

    for(i=0; i<wgoNum; i++) {
        CmdData[i*2] = wgo[i];
        CmdData[i*2+1] = 0;
    }
    res = nF_intf_write_command_u32(CMD_ID_WES, 2*wgoNum, CmdData); // WGO to event: 0
    if( res < 0) return res;

    for(i=0; i<wgoNum; i++) {
        CmdData[i*2] = wgo[i];
        CmdData[i*2+1] = 1;
    }
    res = nF_intf_write_command_u32(CMD_ID_WGO, 2*wgoNum, CmdData); // WGO enable
    if( res < 0) return res;

    return 0;
}

int i_stop_wgo(int wgo[], int wgoNum, float *pfTgt)
{
    int res;
    int i;
    u32 CmdData[16];
 
    // Set target to avoid 'jumps'
    float tgt[MAX_WGO_NUM];
    int axis[MAX_WGO_NUM], svo[MAX_WGO_NUM];
    for(i=0; i<wgoNum; i++) { axis[i] = wgo[i]; }

    if(!pfTgt ) nF_get_dev_axis_target_use(wgoNum, &axis[0], &tgt[0]);
    else { 
        for(i=0; i<wgoNum; i++) tgt[i] = pfTgt[i];
    }

    // Set target to current controlling value
    nF_get_dev_axis_svo(wgoNum, &axis[0], &svo[0]);
    for(i=0; i<wgoNum; i++) {
        nF_set_dev_axis_target(svo[i], 1, &axis[i], &tgt[i]);
    }

    CmdData[0] = 0;
    CmdData[1] = 0; // disable
    res = nF_intf_write_command_u32(CMD_ID_EST, 2, CmdData); // Reset event
    if( res < 0) return res;

    for(i=0; i<wgoNum; i++) {
        CmdData[i*2] = wgo[i];
        CmdData[i*2+1] = 0;
    }

    res = nF_intf_write_command_u32(CMD_ID_WGO, 2*wgoNum, CmdData); // Stop WGO
    if( res < 0) return res;

    res = nF_intf_write_command_u32(CMD_ID_WGC, 2*wgoNum, CmdData); // restore cycle to 0
    if( res < 0) return res;

    res = nF_intf_write_command_u32(CMD_ID_TSL, 2*wgoNum, CmdData); // Target to 'command'
    if( res < 0) return res;

    return 0;
}

int i_rec_cfg(int *rec, int *src, int *ch, int num)
{
    int i, res;
    u32 CmdData[16];

    CmdData[0] = 0;
    CmdData[1] = 0; // clear
    res = nF_intf_write_command_u32(CMD_ID_EST, 2, CmdData); // Reset event
    if( res < 0) return res;

    // recorder setup: connect to source & channel
    for(i=0; i<num; i++) {
        CmdData[0+i*3] = (u8)rec[i];
        CmdData[1+i*3] = (u8)src[i];
        CmdData[2+i*3] = (u8)ch[i];
    }
    res = nF_intf_write_command_u32(CMD_ID_RCF, num*3, CmdData); // Config Recorder-table
    if( res < 0) return res;

    res = nF_intf_write_command_u32(CMD_ID_RCR, 0, CmdData); // Clear Recorder-table
    if( res < 0) return res;

    CmdData[0] = 0;
    CmdData[1] = 0; // event 0
    res = nF_intf_write_command_u32(CMD_ID_RES, 2, CmdData); // Recorder to event: 0
    if( res < 0) return res;

    CmdData[0] = 0;
    CmdData[1] = 1; // enable
    res = nF_intf_write_command_u32(CMD_ID_REN, 2, CmdData); // Enable Recorder
    if( res < 0) return res;

    return 0;
}

int i_start_event(int event[], int eventNum)
{
    int res;
    u32 CmdData[16];

    CmdData[0] = 0;
    CmdData[1] = 1; // enable
    res = nF_intf_write_command_u32(CMD_ID_EEN, 2, CmdData); // Enable event
    if( res < 0) return res;

    CmdData[0] = 0;
    CmdData[1] = 1; // set
    res = nF_intf_write_command_u32(CMD_ID_EST, 2, CmdData); // Set event
    if( res < 0) return res;

    return 0;
}
