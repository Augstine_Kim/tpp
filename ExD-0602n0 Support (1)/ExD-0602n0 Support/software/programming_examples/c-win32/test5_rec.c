
/* Copyright (C) nanoFaktur GmbH - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Cunman Zhang <cunman.zhang@nanoFaktur.com>, Jul. 2020
 */

#include <windows.h>
#include <stdio.h>
#include <string.h>
#include "nF_interface.h"
#include <stdlib.h>

int main(void)
{
    int fd, res; 

    printf("DLL revision: %f\n", nF_get_dll_revision() );
    nF_set_log_state(1); // enable command log: check nF_interface_log.txt for details

    fd = nF_intf_connect_local("EBD-120230");
    if( fd < 0) {
        printf("Can't connect to interface.\n");
        return fd; 
    }
    printf("Interface connected.\n");

	RecoderMemCfg recMemCfg;
	res = nF_get_dev_rec_mem_cfg(&recMemCfg);
	printf("res:%d, MemCfg: (%ld, %ld), (%ld, %ld), event:%d \n", res,
		recMemCfg.TableNum_A, recMemCfg.TableSize_A, recMemCfg.TableNum_B, recMemCfg.TableSize_B, recMemCfg.EvtNo );

	recMemCfg.TableNum_A = 3;
	recMemCfg.TableSize_A = 8192;
	recMemCfg.TableNum_B = 0;
	recMemCfg.TableSize_B = 0;
    recMemCfg.EvtNo = 1;

	res = nF_set_dev_rec_mem_cfg(recMemCfg);
	printf("nF_set_dev_rec_mem_cfg() res:%d\n", res);

	RecoderCfg recCfg;
	recCfg.rate = 10; // sample rate: 100k/10
	recCfg.size = 8192;
	recCfg.num = 2;

	recCfg.rec[0] = 0; // 1st recorder channel
	recCfg.src[0] = 1; // source: REC_SRC_POS
	recCfg.ch[0] = 0;  // from axis0

	recCfg.rec[1] = 1; // 2nd recorder channel
	recCfg.src[1] = 2; // source: REC_SRC_TGT
	recCfg.ch[1] = 0;  // from axis0

	res = nF_set_dev_rec_cfg(recCfg);
	printf("nF_set_dev_rec_cfg() res:%d\n", res);

	res = nF_get_dev_rec_cfg(&recCfg);
	printf("nF_get_dev_rec_cfg() res:%d\n", res);

	res = nF_start_dev_rec();
	printf("nF_start_dev_rec() res:%d\n", res);

	if(res == 0) {
		int i, num;
		float dData[16];

		while(1) {
			num = nF_get_dev_rec_length(0);
			if (num>= recCfg.size) break;
		}

		// Get data
		nF_get_dev_rec_data(0, 0, 16, &dData[0]);
		printf("Data0:\n");
		for(i=0; i<16; i++) printf("%f ", dData[i]);
		printf("\n");

		nF_get_dev_rec_data(1, 8100, 16, &dData[0]);
		printf("Data1:\n");
		for(i=0; i<16; i++) printf("%f ", dData[i]);
		printf("\n");
	}

    nF_intf_disconnect(fd);

    getchar();
    return 0;
}

