
/* Copyright (C) nanoFaktur GmbH - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Cunman Zhang <cunman.zhang@nanoFaktur.com>, Feb. 2018
 */

#include <windows.h>
#include <stdio.h>
#include <conio.h>  	// for getch()
#include <string.h>
#include "nF_interface.h"

#define MAX_AXIS       1  // for 1-channel-controller
//#define MAX_AXIS     2  // for 2-channels-controller
//#define MAX_AXIS     3  // for 3-channels-controller

#define OPEN_LOOP    0
#define CLOSE_LOOP   1

void test_axis_range(void);
void test_axis_func(void);
void test_param_info(int fd);
void test_param_func(void);

//#define USE_RS232
//#define USE_ETHERNET
#define USE_USB
 
int main(void)
{
    int fd; 
    int res=0;
    CommandCfg cmdCfg;
    ResponseMsg *pRspMsg=NULL;

    printf("DLL revision: %f\n", nF_get_dll_revision() );
    nF_set_log_state(1); // enable command log

#if defined(USE_RS232) // serial-interface (COM-port)
    fd = nF_intf_connect_com(1);           // Use COM1
#else
    char ipaddr[16];
#if defined(USE_ETHERNET)
    char udpRsp[256];
    res = nF_udp_search(udpRsp, 256, 2000); // search UDP controller (timeout set to 2s)
    if(res) {
        printf("nF_udp_search() returned with error-code:%d\n", res);
        return res;
    }
    printf("Response from controller: %s\n", udpRsp);
    res = nF_udp_rsp_to_ipaddr( (const char*)udpRsp, ipaddr, 16);
    if(res) {
        printf("nF_udp_rsp_to_ipaddr() returned with error-code:%d\n", res);
        return res;
    }
#else //if defined(USE_USB)
    sprintf(ipaddr, "192.168.168.168");    // USB uses this IP, (windows driver is RNDIS)
#endif
    printf("IPAddress: %s\n", ipaddr);
    fd = nF_intf_connect_tcpip(ipaddr);    // controller uses IP port: 51000
#endif

    if( fd < 0) {
        printf("Can't connect to interface.\n");
        goto test_end; 
    }
    printf("Interface connected.\n");

    // Example for reading from controller
    memset(&cmdCfg, 0x00, sizeof(CommandCfg) );
    cmdCfg.cmdId = 0x1000; // command ID: pop-error 
    cmdCfg.customId = 0x2018; // what ever ...
    cmdCfg.option = CMD_PACKAGE_FLAG_RD | CMD_PACKAGE_FLAG_ACK;
    cmdCfg.dataNum = 0; // No parameters 

    // use buffer (pRspMsg) from DLL 
    res = nF_intf_read_command(fd, &cmdCfg, &pRspMsg);
    while( res == RSP_PACKAGE_MORE) {
        printf("Controller responses: (Number of data:%d)\n", pRspMsg->dataNum);
        res = nF_intf_read_more(fd, &pRspMsg);
    }
    printf("Controller responses: %d\n", res);
    if(res < 0) goto test_end;
    nF_free_rspMsg(pRspMsg);

    test_axis_range();
    test_axis_func();
    test_param_info(fd);
    test_param_func();

    nF_intf_disconnect(fd);

test_end:
    printf("Press any key to quit.\n");
    getch();

    return res;
}

void test_axis_range(void)
{
    int i, err;
    int axis[MAX_AXIS];
    float fData[10];
    printf("%s()...\n", __func__);

    for(i=0; i<MAX_AXIS; i++) axis[i] = i;
    for(i=0; i<MAX_AXIS; i++) fData[i] = -20.0;
    err = nF_set_dev_axis_range_low(OPEN_LOOP, MAX_AXIS, axis, fData);
    if( err ) printf("Error with nF_set_dev_axis_range_low() Error:%d\n", err);

    for(i=0; i<MAX_AXIS; i++) fData[i] = 120.0;
    err = nF_set_dev_axis_range_high(OPEN_LOOP, MAX_AXIS, axis, fData);
    if( err ) printf("Error with nF_set_dev_axis_range_high() Error:%d\n", err);

    err = nF_get_dev_axis_range_low(OPEN_LOOP, MAX_AXIS, axis, fData);
    if( !err ){
        for(i=0; i<MAX_AXIS; i++) printf("Axis: %d, open-loop low-limit: %f\n", axis[i], fData[i]);
    }
    else printf("Error with nF_get_dev_axis_range_low() Error:%d\n", err);

    err = nF_get_dev_axis_range_high(OPEN_LOOP, MAX_AXIS, axis, fData);
    if( !err ){
        for(i=0; i<MAX_AXIS; i++) printf("Axis: %d, open-loop high-limit: %f\n", axis[i], fData[i]);
    }
    else printf("Error with nF_get_dev_axis_range_high() Error:%d\n", err);
    
    err = nF_get_dev_axis_range_low(CLOSE_LOOP, MAX_AXIS, axis, fData);
    if( !err ){
        for(i=0; i<MAX_AXIS; i++) printf("Axis: %d, close-loop low-limit: %f\n", axis[i], fData[i]);
    }
    else printf("Error with nF_get_dev_axis_range_low() Error:%d\n", err);

    err = nF_get_dev_axis_range_high(CLOSE_LOOP, MAX_AXIS, axis, fData);
    if( !err ){
        for(i=0; i<MAX_AXIS; i++) printf("Axis: %d, close-loop high-limit: %f\n", axis[i], fData[i]);
    }
    else printf("Error with nF_get_dev_axis_range_high() Error:%d\n", err);
}

void test_axis_func(void)
{
    int i, err;
    int axis[MAX_AXIS];
    float fData[10];
    int uData[MAX_AXIS];
    printf("%s()...\n", __func__);

    err = nF_get_dev_error();
    printf("Last error code: %d\n", err);

    for(i=0; i<MAX_AXIS; i++) axis[i] = i;

    err = nF_get_dev_axis_target_use(1, &axis[0], fData);
    if( !err ){
        printf("Axis: %d, Target: %f\n", axis[0], fData[0]);
    }
    else printf("Error with nF_get_dev_axis_target() Error:%d\n", err);
    
    err = nF_get_dev_axis_target(CLOSE_LOOP, MAX_AXIS, axis, fData);
    if( !err ){
        for(i=0; i<MAX_AXIS; i++) printf("Axis: %d, closeloop Target: %f\n", axis[i], fData[i]);
    }
    else printf("Error with nF_get_dev_axis_target() Error:%d\n", err);

    err = nF_get_dev_axis_target(OPEN_LOOP, MAX_AXIS, axis, fData);
    if( !err ){
        for(i=0; i<MAX_AXIS; i++) printf("Axis: %d, openloop Target: %f\n", axis[i], fData[i]);
    }
    else printf("Error with nF_get_dev_axis_target() Error:%d\n", err);

    err = nF_get_dev_axis_position(MAX_AXIS, axis, fData);
    if( !err ){
        for(i=0; i<MAX_AXIS; i++) printf("Axis: %d, Position: %f\n", axis[i], fData[i]);
    }
    else printf("Error with nF_get_dev_axis_position() Error:%d\n", err);

    for(i=0; i<MAX_AXIS; i++) fData[i] = 1.0 + i*5.0;
    err = nF_set_dev_axis_target(OPEN_LOOP, MAX_AXIS, axis, fData); // Open loop target
    if( err ) printf("Error with nF_set_dev_axis_target() Error:%d\n", err);
    
    for(i=0; i<MAX_AXIS; i++) fData[i] = 10.0 + i*5.0;
    err = nF_set_dev_axis_target(1, MAX_AXIS, axis, fData); // Close loop target
    if( err ) printf("Error with nF_set_dev_axis_target() Error:%d\n", err);

    for(i=0; i<MAX_AXIS; i++) uData[i] = CLOSE_LOOP; 
    err = nF_set_dev_axis_svo(MAX_AXIS, axis, uData); // servo-on/off
    if( err ) printf("Error with nF_set_dev_axis_svo() Error:%d\n", err);

    err = nF_get_dev_axis_svo(MAX_AXIS, axis, uData);
    if( !err ){
        for(i=0; i<MAX_AXIS; i++) printf("Axis: %d, SVO-status: %d\n", axis[i], uData[i]);
    }
    else printf("Error with nF_get_dev_axis_svo() Error:%d\n", err);

    err = nF_get_dev_axis_ont(MAX_AXIS, axis, uData);
    if( !err ){
        for(i=0; i<MAX_AXIS; i++) printf("Axis: %d, ONT-status: %d\n", axis[i], uData[i]);
    }
    else printf("Error with nF_get_dev_axis_ont() Error:%d\n", err);

    err = nF_get_dev_axis_ovf(MAX_AXIS, axis, uData);
    if( !err ){
        for(i=0; i<MAX_AXIS; i++) printf("Axis: %d, OVF-status: %d\n", axis[i], uData[i]);
    }
    else printf("Error with nF_get_dev_axis_ovf() Error:%d\n", err);

    err = nF_get_dev_error();
    printf("Controller error code: %d\n", err);
}

void test_param_info(int fd)
{   
    CommandCfg cmdCfg;
    ResponseMsg *pRspMsg=NULL;
    int i, j, res;
    u8 fmt;
    u32 uData;

    // Set command-level to 1
    fmt = CMD_DATA_FMT_U32;
    uData = 1;
    cmdCfg.cmdId = 0xFFF0;
    cmdCfg.customId = 0x0101;
    cmdCfg.dataNum = 1;
    cmdCfg.pDataFmt = &fmt;
    cmdCfg.pDataIn = &uData;
    res = nF_intf_write_command(fd, &cmdCfg);

    // Get parameter-information
    cmdCfg.cmdId = 0xFFFE;
    cmdCfg.customId = 0x0101;
    cmdCfg.option = CMD_PACKAGE_FLAG_RD;
    cmdCfg.dataNum = 0;

    j = 0;
    res = nF_intf_read_command(fd, &cmdCfg, &pRspMsg);
    while(1) {
        if( res < 0) break;
        // parser response

        // U32: paramterId
        // U32: read format
        // U32: write format
        // string: parameter-help
         for(i=0; i<pRspMsg->dataNum; i++) {
            if(j==0) printf("paramterId: 0x%08lx\t", pRspMsg->pDataOut[i]);
            else if(j==1) printf("ch/flag: 0x%08lx\t", pRspMsg->pDataOut[i]);
            else if(j==2) printf("Read: 0x%08lx\t", pRspMsg->pDataOut[i]);
            else if(j==3) printf("Write: 0x%08lx\t", pRspMsg->pDataOut[i]);
            else if(j==4) printf("Help: %s", (char*)pRspMsg->pDataOut[i]);
            else /*j==5*/ printf("\n");

            if(++j>=6) j = 0;
        }

        if( res == RSP_PACKAGE_END) break;
        res = nF_intf_read_more(fd, &pRspMsg);
    }
    nF_free_rspMsg(pRspMsg);
}

void test_param_func(void)
{
    int err, ch;
    u8 fmt;
    ParamVal val;
    printf("%s()...\n", __func__);

    err = nF_get_dev_error();
    printf("Last error code: %d\n", err);

    ch = 0;
    err = nF_get_dev_parameter_ram(ch, 0x20400100, &fmt, &val );    // P-term
    if( err ) printf("Error with nF_get_dev_parameter_ram() Error:%d\n", err);
    else printf("ch:%d, RAM-P-term: %f\n", ch, val.fData);

    fmt = CMD_DATA_FMT_FLOAT;
    val.fData = 0.001;
    //err = nF_set_dev_parameter_ram(ch, 0x20400100, fmt, &val );    // P-term
    if( err ) printf("Error with nF_set_dev_parameter_ram() Error:%d\n", err);

    if(MAX_AXIS>1) {
    ch = 1;
    err = nF_get_dev_parameter_ram(ch, 0x20400101, &fmt, &val );    // I-term
    if( err ) printf("Error with nF_get_dev_parameter_ram() Error:%d\n", err);
    else printf("ch:%d, RAM-I-term: %f\n", ch, val.fData);
    }
   
    if(MAX_AXIS>2) {
    ch = 2;
    err = nF_get_dev_parameter_ram(ch, 0x20400102, &fmt, &val );    // D-term
    if( err ) printf("Error with nF_get_dev_parameter_ram() Error:%d\n", err);
    else printf("ch:%d, RAM-D-term: %f\n", ch, val.fData);
    }

    ch = 0;
    err = nF_get_dev_parameter_flash(ch, 0x20400100, &fmt, &val );    // P-term
    if( err ) printf("Error with nF_get_dev_parameter_flash() Error:%d\n", err);
    else printf("ch:%d, FLASH-P-term: %f\n", ch, val.fData);

    // Axis name
    ch = 0;
    fmt = CMD_DATA_FMT_STRING;
    sprintf(val.sData, "AxisX");
    //err = nF_set_dev_parameter_flash(ch, 0x20000001, fmt, &val );    // axis name
    if( err ) printf("Error with nF_set_dev_parameter_flash() Error:%d\n", err);

    ch = 0;
    err = nF_get_dev_parameter_flash(ch, 0x20000001, &fmt, &val );    
    if( err ) printf("Error with nF_get_dev_parameter_flash() Error:%d\n", err);
    else printf("ch:%d, FLASH-Axis-name: %s\n", ch, val.sData );

    if(MAX_AXIS>1) {
    ch = 1;
    err = nF_get_dev_parameter_default(ch, 0x20400102, &fmt, (void*)&val );    // D-term
    if( err ) printf("Error with nF_get_dev_parameter_default() Error:%d\n", err);
    else printf("ch:%d, Default-D-term: %f\n", ch, val.fData);
    }

    err = nF_get_dev_error();
    printf("Last error code: %d\n", err);
}


