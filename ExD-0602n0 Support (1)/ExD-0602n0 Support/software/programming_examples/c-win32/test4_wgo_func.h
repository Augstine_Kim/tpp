#ifndef _WGO_H_ 
#define _WGO_H_

#define SVO_RATE_KHZ             100
#define MAX_DATA_LEN             16384
#define MIN_DATA_LEN             100

#define WAVE_FORM_PNT            0
#define WAVE_FORM_SIN            1
#define WAVE_FORM_SQUARE         3
#define WAVE_FORM_TRIANGLE       5

#ifndef MAX_WGO_NUM
#define MAX_WGO_NUM              3
#endif

#ifndef MAX_SEL_REC_NUM
#define MAX_SEL_REC_NUM          16
#endif

#define CMD_ID_TSL  0x2041 // Target selection
#define CMD_ID_HVE  0x22fe // HV enable
#define CMD_ID_WCR  0x3000 // wave table clear
#define CMD_ID_WAV  0x3001 // wave table set
#define CMD_ID_WTC  0x3010 // wave table configure
#define CMD_ID_WSL  0x3012 // wave table selection
#define CMD_ID_WGO  0x3140 // generator enable
#define CMD_ID_WGC  0x3142 // generator cycle
#define CMD_ID_WES  0x3151 // generator event selection

#define CMD_ID_RCR  0x4000 // recorder table clear
#define CMD_ID_RTC  0x4010 // recorder table configure
#define CMD_ID_REN  0x4040 // recorder enable
#define CMD_ID_DRL  0x4042 // recorder table length
#define CMD_ID_RCF  0x4050 // configure recorder table
#define CMD_ID_RES  0x4051 // recorder event selection

#define CMD_ID_EEN  0xD041 // event enable
#define CMD_ID_EST  0xD042 // event set

extern int i_time2_len_rate(double time_ms, int *pLen, int *pRate);
extern int i_set_wgo_rate(int ch, int rate);
extern int i_set_rec_rate(int rate);
extern int i_wave_cfg(int waveId, int form, int len, float *param, int paramNum);
extern int i_wave_cfg_pnt(int waveId, int from, int len, float *param);
extern int i_wgo_cfg(int *wgo, int *wave, int wgoNum);
extern int i_stop_wgo(int wgo[], int wgoNum, float *pfTgt);
extern int i_rec_cfg(int *rec, int *src, int *ch, int num);
extern int i_start_event(int event[], int eventNum);

#endif // #ifndef _WGO_H_
