from ctypes import*

CMD_ID_TSL = 0x2041 # Target selection 
CMD_ID_WCR = 0x3000 # wave table clear
CMD_ID_WAV = 0x3001 # define wave table
CMD_ID_WSL = 0x3012 # wave table selection
CMD_ID_WGO = 0x3140 # wgo enable
CMD_ID_WGC = 0x3142 # wgo cycles
CMD_ID_WES = 0x3151 # wgo event selection
CMD_ID_HVE = 0x22FE # High-voltage output enable

CMD_ID_RCR = 0x4000 # recorder table clear
CMD_ID_REN = 0x4040 # recorder enable
CMD_ID_RCF = 0x4050 # configure recorder table
CMD_ID_RES = 0x4051 # recorder event selection
CMD_ID_EEN = 0xD041 # event enable
CMD_ID_EST = 0xD042 # event set

handle = 0
intfFd = 0

def xDev_init(handle0, sIntf, sParam):
    # Function: check DLL and connect to device
    
    global handle         # the handle of nF_interface.dll 
    #print("xDev_init()...")
    handle = handle0
    func_rev = handle.nF_get_dll_revision
    func_rev.restype = c_float
    print("nF_interface.DLL rev. is: %5.2f." % func_rev())
    res = handle.nF_get_dll_last_error();
    print("nF_interface.DLL error code: %d" % res)
    res = xDev_connect_intf(sIntf, sParam)
    if res != 0: return res
    res = handle.nF_get_dev_error();
    print("Device error code: %d" % res)
    if res <= -2000: #Software problem
        print("Connection failed!")
        return 1
    return 0

def xDev_exit():
    global intfFd, handle
    #print("xDev_exit...")
    if (handle !=0 ) and intfFd >=0 :
        handle.nF_intf_disconnect(intfFd);
    print("Device interface disconnected.")
    return 0

def xDev_connect_intf(sIntf, sParam):
    global intfFd, handle

    print("Selected interface: %s, parameter: %s" % (sIntf.decode(), sParam.decode()) )
    sIntfCmp = sIntf.lower()
    if (sIntfCmp== b'rs232') or (sIntfCmp==b'com')  or (sIntfCmp==b'uart'):
        #------------------------------------------
        #For RS232 interface
        #------------------------------------------
        func_connect = handle.nF_intf_connect_com
        intfFd = func_connect(int(sParam ))
    elif sIntfCmp== b'simu' :
        #------------------------------------------
        # for simulation
        #------------------------------------------
        func_connect = handle.nF_intf_connect_local
        arg = create_string_buffer(b"EBD-1202x0")
        func_connect.argtypes = [c_char_p]
        intfFd = func_connect(arg)
    else :
        #------------------------------------------
        # for USB & TCP/IP
        #------------------------------------------
        func_connect = handle.nF_intf_connect_tcpip
        #arg = create_string_buffer(b"192.168.178.2") # connect to Ethernet
        # TODO: TCP/IP not implemented yet
        arg = create_string_buffer(b"192.168.168.168") # connect to USB
        func_connect.argtypes = [c_char_p]
        intfFd = func_connect(arg)

    if intfFd < 0 :
        res = handle.nF_get_sys_last_error();
        print("DLL system-error code: %d" % res)
        print("Connection failed!")
        return 1
    
    #print("interface intfFd:%d" % intfFd)
    print("Device interface connected.")
    return 0

def xDev_write_command(cmdId, parNum, par):
    #U32 command
    #   e.g 0x2040 0 1
    #   cmdId = 0x2040
    #   parNum = 2
    #   par[0] = 0
    #   par[1] = 1
    global handle
    handle.nF_intf_write_command_u32(cmdId, parNum, par)

def xDev_read_command_u32(cmdId, parNum, par, rsp):
    #U32 command
    #   e.g ?0x2040 0 1
    #   cmdId = 0x2040
    #   parNum = 2
    #   par[0] = 0
    #   par[1] = 1
    # Output:
    global handle
    rspNum = c_int(0)
    handle.nF_intf_read_command_u32(cmdId, parNum, par, byref(rspNum), rsp)
    return rspNum.value;

def xDev_pop_error():
    #pop (read-back and clear) device error-code
    global handle
    res = handle.nF_get_dev_error();
    #print("Device error code: %d" % res)
    return res

def xDev_get_svo(axis):
    #Get axis servo-status
    global handle
    ch = c_int(axis) 
    svo = c_int(0)
    handle.nF_get_dev_axis_svo(1, byref(ch), byref(svo) )
    return svo.value

def xDev_set_svo(axis, bSvo):
    #Set axis servo-controlling
    global handle
    ch = c_int(axis) 
    svo = c_int(bSvo) 
    return handle.nF_set_dev_axis_svo(1, byref(ch), byref(svo) )

def xDev_set_svo_softly(axis, bOn) :
    # No jump when change SVO status
    pos = xDev_get_pos(axis)
    cVol = xDev_get_cVol(axis);
    
    if bOn :
        xDev_move(axis, pos);
    else :
        xDev_sva(axis, cVol);
        
    xDev_set_svo(axis, bOn)    
    return 0;
    
def xDev_get_ont(axis):
    #Get axis on-target-status
    global handle
    ch = c_int(axis) 
    ont = c_int(0)
    handle.nF_get_dev_axis_ont(1, byref(ch), byref(ont) )
    return ont.value

def xDev_get_pos(axis):
    #Get axis position
    global handle
    ch = c_int(axis) 
    pos = c_float(0.0) 
    handle.nF_get_dev_axis_position(1, byref(ch), byref(pos) );
    return pos.value

def xDev_get_tgt(axis, svo) :
    #gGet axis target (svo=1 for close-loop, 0 for open-loop)
    global handle
    ch = c_int(axis) 
    tgt = c_float(0.0) 
    handle.nF_get_dev_axis_target(svo, 1, byref(ch), byref(tgt) )
    return tgt.value
    
def xDev_get_cVol(axis) :
    # Get control-voltage of controller
    global handle
    ch = c_int(axis) 
    cVol = c_float(0.0) 
    handle.nF_get_dev_axis_cVol(1, byref(ch), byref(cVol) )
    return cVol.value

def xDev_move(axis, tgt):
    #Set axis close-loop target
    global handle
    ch = c_int(axis) 
    val = c_float(tgt) 
    return handle.nF_set_dev_axis_target(1, 1, byref(ch), byref(val) )

def xDev_sva(axis, tgt):
    #Set axis open-loop target
    global handle
    ch = c_int(axis) 
    val = c_float(tgt) 
    return handle.nF_set_dev_axis_target(0, 1, byref(ch), byref(val) )
    
def xDev_get_param_int(ch, parId):
    #Get parameter (integer type)
    global handle
    arg1 = c_int(ch)
    arg2 = c_long(parId)
    arg3 = c_int(0) #fmt
    arg4 = c_int() #value
    handle.nF_get_dev_parameter_ram(arg1, arg2, byref(arg3), byref(arg4) )
    return arg4.value

def xDev_get_param_float(ch, parId):
    #Get parameter (float type)
    global handle
    arg1 = c_int(ch)
    arg2 = c_long(parId)
    arg3 = c_int(0) #fmt
    arg4 = c_float() #value
    handle.nF_get_dev_parameter_ram(arg1, arg2, byref(arg3), byref(arg4) )
    return arg4.value

def xDev_get_param_string(ch, parId):
    #Get parameter (string type)
    global handle
    arg1 = c_int(ch)
    arg2 = c_long(parId)
    arg3 = c_int(4) #fmt
    arg4 = create_string_buffer(32) #value
    handle.nF_get_dev_parameter_ram(arg1, arg2, byref(arg3), byref(arg4) )
    return arg4.value

def xDev_set_param_float(ch, parId, val):
    #Set parameter (can be used for float/int type)
    global handle
    arg1 = c_int(ch)
    arg2 = c_long(parId)
    arg3 = c_float(val) 
    return handle.nF_set_dev_parameter_ram(arg1, arg2, 2, byref(arg3) )
    
def xDev_set_param_string(ch, parId, sVal):
    # Set parameter (string type)
    global handle
    #print("String input is:%s" % sVal)
    arg1 = c_int(ch)
    arg2 = c_long(parId)
    arg3 = create_string_buffer( sVal.encode('utf-8') )
    return handle.nF_set_dev_parameter_ram(arg1, arg2, 4, byref(arg3) )

def xDev_set_param_float_flash(ch, parId, val):
    #Set parameter to flash(can be used for float/int type)
    global handle
    arg1 = c_int(ch)
    arg2 = c_long(parId)
    arg3 = c_float(val) 
    return handle.nF_set_dev_parameter_flash(arg1, arg2, 2, byref(arg3) )

def xDev_get_AIN_vol(ch) :
    #Get analog-input-voltage (command: 0x2114)
    global handle
    argIn = c_int(ch)
    rsp = (c_float*2)()
    num = c_int()
    handle.nF_intf_read_command_u32(0x2114, 1, byref(argIn), byref(num), byref(rsp) )
    #print("rsp num:%d" % num.value)
    return rsp[1]

def xDev_get_monitor_HV(ch) :
    #Get monitor value of high-voltage (command: 0x2116, piezo-voltage)
    # for high-end only
    global intfFd, handle
    argIn = c_int(ch*2)
    rsp = (c_float*2)()
    num = c_int()
    handle.nF_intf_read_command_u32(0x2116, 1, byref(argIn), byref(num), byref(rsp) )
    #print("rsp num:%d" % num.value)
    return rsp[1]

def xDev_get_monitor_vol(ch) :
    #Get monitor voltage (command: 0x2116, aux-voltage)
    # for high-end only
    global handle
    argIn = c_int(ch*2 + 1)
    rsp = (c_float*2)()
    num = c_int()
    handle.nF_intf_read_command_u32(0x2116, 1, byref(argIn), byref(num), byref(rsp) )
    #print("rsp num:%d" % num.value)
    return rsp[1]

def xDev_set_monitor_src(ch, src) :
    #select monitor source
    args = (c_int*2)()
    args[0] = c_int(ch)
    args[1] = c_int(src)
    print("Monitor source selection ch:%d src:%d" % (args[0], args[1]) )
    xDev_write_command(0x21FE, 2, byref(args) )
    return 0;

def xDev_start_rec(rate, ch) : 
    # start data recorder
    #   rate: recorder rate (save data in how many servo-loops)
    #   ch: position of which channel for saving
    global handle
    argIn = (c_int*3)()

    # cancel old action: disable-event
    argIn[0] = 0
    argIn[1] = 0
    handle.nF_intf_write_command_u32(CMD_ID_EEN, 2, byref(argIn) )
    handle.nF_intf_write_command_u32(CMD_ID_EST, 2, byref(argIn) )

    # clear recorder table
    handle.nF_intf_write_command_u32(CMD_ID_RCR, 0, byref(argIn) )

    # set recorder rate
    xDev_set_param_float(0, 0x40400000, rate)
    
    # set source to position (ch)
    argIn[0] = 0 # recorder 0
    argIn[1] = 1 # source = position, change to get other data
    argIn[2] = ch # channel
    handle.nF_intf_write_command_u32(CMD_ID_RCF, 3, byref(argIn) )

    # recorder to event0
    argIn[0] = 0
    argIn[1] = 0
    handle.nF_intf_write_command_u32(CMD_ID_RES, 2, byref(argIn) )

    # enable recorder
    argIn[0] = 0
    argIn[1] = 1
    handle.nF_intf_write_command_u32(CMD_ID_REN, 2, byref(argIn) )

    # enable event
    argIn[0] = 0
    argIn[1] = 1
    handle.nF_intf_write_command_u32(CMD_ID_EEN, 2, byref(argIn) )

    # set event: recorder then starts
    argIn[0] = 0
    argIn[1] = 1
    handle.nF_intf_write_command_u32(CMD_ID_EST, 2, byref(argIn) )

    return 0;

def xDev_get_rec_data(dataLen, bufAddr) : 
    global handle
    recCh = 0
    recFrom = 0

    #check whether all data valid
    valid = handle.nF_get_dev_rec_length(recCh)
    if valid < dataLen :
        return -1 - valid;
    
    handle.nF_get_dev_rec_data(recCh, recFrom, dataLen, bufAddr)
    return 0

def xDev_start_sin(ch):
    # Function: start a sin-wave form:
    # For High-end controller only
    global handle
    argIn = (c_int*4)()
    argInF = (c_float*9)()

    offset = -20.0    # from
    vMax = 120.0      # to
    freq = 20.0
    phase = 0.0
    cycle = 1200    # number of cycles
    
    # cancel old action: disable-event
    argIn[0] = 0
    argIn[1] = 0
    handle.nF_intf_write_command_u32(CMD_ID_EEN, 2, byref(argIn) )
    handle.nF_intf_write_command_u32(CMD_ID_EST, 2, byref(argIn) )

    # 20Hz = 50ms, for 100k-SPS, total 5000 points
    wLen = 100000.0/freq # number of data
    rate = 1
    xDev_set_param_float(ch, 0x31400000, rate)

    # Clear wave-table
    argIn[0] = ch   # channel
    handle.nF_intf_write_command_u32(CMD_ID_WCR, 1, byref(argIn) )

    # Set cycles
    argIn[0] = ch   # channel
    argIn[1] = cycle
    handle.nF_intf_write_command_u32(CMD_ID_WGC, 2, byref(argIn) )

    # Wave data
    argInF[0] = float(ch)   # which channel
    argInF[1] = float(0)    # mode = new
    argInF[2] = float(1)    # SIN form
    argInF[3] = float(0)    # x0
    argInF[4] = float(wLen) # wave-segLength
    argInF[5] = vMax - offset # amplitude
    argInF[6] = wLen        # length
    argInF[7] = offset      # Offset
    argInF[8] = phase
    handle.nF_intf_write_command_float(CMD_ID_WAV, 9, byref(argInF) )

    # wgo to wave-data-table
    argIn[0] = ch   # wgo channel
    argIn[1] = ch   # wave data table
    handle.nF_intf_write_command_u32(CMD_ID_WSL, 2, byref(argIn) )
    
    # wgo event selection
    argIn[0] = ch   # wgo channel
    argIn[1] = 0    # to event# 0
    handle.nF_intf_write_command_u32(CMD_ID_WES, 2, byref(argIn) )
    
    # HV output enable
    argIn[0] = ch   # channel
    argIn[1] = 1
    handle.nF_intf_write_command_u32(CMD_ID_HVE, 2, byref(argIn) )

    # Target selction: to WGO
    argIn[0] = ch   # channel
    argIn[1] = 1
    handle.nF_intf_write_command_u32(CMD_ID_TSL, 2, byref(argIn) )
    
    # wgo enable
    argIn[0] = ch   # channel
    argIn[1] = 1
    handle.nF_intf_write_command_u32(CMD_ID_WGO, 2, byref(argIn) )

    # enable event
    argIn[0] = 0    # event# 0
    argIn[1] = 1
    handle.nF_intf_write_command_u32(CMD_ID_EEN, 2, byref(argIn) )

    # set event: wgo starts
    argIn[0] = 0    # event# 0
    argIn[1] = 1
    handle.nF_intf_write_command_u32(CMD_ID_EST, 2, byref(argIn) )

    return 0

#--------------------------------------------------------------
#    Read & write command
#--------------------------------------------------------------
class _CMD_CFG_(Structure) :
    _fields_ = ("cmdId", c_ushort), ("customId", c_ushort), \
        ("option", c_ubyte), ("dataNum", c_int), \
        ("pDataFmt", POINTER(c_ubyte)), ("pDataIn", POINTER(c_uint))

class _RSP_MSG_(Structure) :
    _fields_ = ("cmdId", c_ushort), ("customId", c_ushort), \
        ("seq", c_int), ("pPackage", c_char_p), ("dataNum", c_int), \
        ("pDataFmt", POINTER(c_ubyte)), ("pDataOut", POINTER(c_uint) )
    
def xDev_test_cmd():
    global intfFd, handle
    #TODO: here without parameters to controller
    #print("Fd: %d" % intfFd)
    
    fmt = (c_ubyte*3)(0, 0, 0)
    axis = (c_uint*3)(0, 0, 0)
    cmdCfg = _CMD_CFG_(c_ushort(0x2001), 0x0000, 0x00, 3) #option can be set to 0x20
    cmdCfg.pDataFmt = cast(fmt, POINTER(c_ubyte) )
    cmdCfg.pDataIn = cast(axis, POINTER(c_uint))
    
    #Function call
    ppRsp = POINTER(_RSP_MSG_)()
    cmd_rd = handle.nF_intf_read_command
    cmd_rd.argtypes = [c_int, POINTER(_CMD_CFG_), POINTER(POINTER(_RSP_MSG_))]
    cmd_rd.restype = c_int
    res = cmd_rd(intfFd, byref(cmdCfg), byref(ppRsp) )

    #Show response
    if res != 0:
        print("Response error: %d" % res)
        return -1
    
    print("Response: res=%d, 0x%04x, num:%d" % (res, ppRsp[0].cmdId, ppRsp[0].dataNum) )
    pFmt = cast(ppRsp[0].pDataFmt, POINTER(c_ubyte) )
    pData = cast(ppRsp[0].pDataOut, POINTER(c_int) )
    for i in range( ppRsp[0].dataNum ) :
        fmt = pFmt[i]
        data = pData[i]
        if fmt == 2 : #float data
            nData = c_int(data)
            fpData = cast(byref(nData), POINTER(c_float) )
            fData = fpData.contents
            print("response format: float, data: %7.3f" % fData.value)
        elif fmt == 10 : # line-feed
            continue
        else :
            print("response format: %d, data:%d" % (fmt, data) )
        
    #free the memory
    rsp_free = handle.nF_free_rspMsg
    rsp_free.argtypes = [POINTER(_RSP_MSG_)]
    rsp_free(ppRsp)
    return 0

def xDev_simple_test():
    global handle
    print("First copy nF_interface.dll to current folder");
    handle = cdll.LoadLibrary("nF_interface.dll") 
    res = xDev_init(handle, b'USB', b'none')
    if res != 0: return res
    axis = 0
    xDev_set_svo(axis, 0)
    xDev_sva(axis, 10.0)
    pos = xDev_get_pos(axis)
    print("servo ch%d: %d"  % (axis, xDev_get_svo(axis)) )
    print("position ch%d: %7.3f" % (axis, pos) )
    print("PID ch:%d, P-Term: %7.3f" % (axis, xDev_get_param_float(axis, 0x20400100) ) )

    xDev_test_cmd()
    
    xDev_exit()
    del handle
    return 0

#xDev_simple_test()
