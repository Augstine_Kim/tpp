
1. For each channel, save data to one file
Data format: 
 Rate=100 
 Data0
 Data1
...

Note: 
* Rate will be set as wave-generator-rate. Servo-update-rate is 100kHz, for Rate = 100, controller outputs data every milisecond.
* Maximal number of data is 16384. 

2. Load data to controller by selecting Waveform "Custom data file".

3. Run wave generator by "Start"
