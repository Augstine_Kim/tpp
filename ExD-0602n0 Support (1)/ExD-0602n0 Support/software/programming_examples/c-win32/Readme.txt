
this is a demo c-source code compiled with mingw32 (please see http://www.mingw.org/ for detail)

test1.c: demo for set-target/get-position (via RS232)
	1. copy the nF_interface.dll to this folder
	2. compile the source file with command: make
	3. power on the controller and connect RS232 cable to PC
	4. run test1.exe

test2.c: demo for connecting to simulation-application
	1. copy the nF_interface.dll to this folder
	2. compile the source file with command: make
	3. start ../nFControl/simulation.bat and Don�t close the window. 
	4. then run the test2.exe

test3.c: demo for connecting to controller (default interface is USB)
	1. copy the nF_interface.dll to this folder
	2. compile the source file with command: make
	3. power on the controller and connect USB cable to PC
	4. install the USB driver (see manual)
	5. run test3.exe

test4_wgo.c: demo for testing wave-generator

test5_rec.c: demo for testing data-recorder


	