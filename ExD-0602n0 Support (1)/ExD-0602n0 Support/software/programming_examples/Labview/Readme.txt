/* Copyright (C) nanoFaktur GmbH - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Written by Cunman Zhang <cunman.zhang@nanoFaktur.com>, Jun. 2020
 */

Example code for Labview-2020 
Tested with windows10-64Bits

DLL used in "vi" is "E:\temp\DLL\nF_interface_x64.dll".
The path should be changed according to DLL location.

Function description:
	1. show DLL revision (optional)
		nF_get_dll_revision()

	2. enable log function (optional)
		nF_set_log_state(1)
		for testing purpose only 
		Data saved in: nF_interface_log.txt

	3. connect to interface
		fd = nF_intf_connect_local("EBD-120230")	
		Note: should be changed to the right interface

	4. set close-loop mode 
		nF_set_dev_axis_svo(num, pAxis, pVal)
		Tested is axis0
		num = 1, // only one channel
		pAxis[0] = 0, // index of axis0
		pVal[0] = 1, // 0=open-loop, 1=close-loop
	loop:
	5. set target
		nF_set_dev_axis_target(bSvo, num, pAxis, pTgt)
		bSvo = 1,
		num = 1, 
		pAxis[0] = 0, // index of axis0
		pTgt[0] = 0.0, // target value
	6. get position of axis0
		nF_get_dev_axis_position(num, pAxis, pVal)
		num = 1, 
		pAxis[0] = 0, // index of axis0
		pVal[0], // returned position value

	7. close interface
		nF_intf_disconnect(fd)

Note: settings for operation of axis0 and axis2
	num = 2
	pAxis[0] = 0, // index of axis0
	pAxis[1] = 2, // index of axis2
	pTgt[0] = 0.0, // target value of axis0
	pTgt[1] = 10.0, // target value of axis2
	call: nF_set_dev_axis_target(bSvo, num, pAxis, pTgt)


Complete.
	